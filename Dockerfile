FROM python:3.10.12

WORKDIR /app

ADD . .

RUN pip install -r requirements.txt

ENV NEPTUNE_PROJECT_NAME="rickyindrag/text-classification"
ENV NEPTUNE_API_TOKEN="eyJhcGlfYWRkcmVzcyI6Imh0dHBzOi8vYXBwLm5lcHR1bmUuYWkiLCJhcGlfdXJsIjoiaHR0cHM6Ly9hcHAubmVwdHVuZS5haSIsImFwaV9rZXkiOiI3NDI1OTdiMC1kNjMyLTQxNDQtYmMxYi0yOGJjM2NkMTI1MzcifQ=="

RUN python preprocess.py

CMD ["python", "app.py"]
